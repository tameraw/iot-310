import time
from gpiozero import MotionSensor

pir = MotionSensor(4)
while True:
    print("Waiting for motion to stop...\r")
    pir.wait_for_no_motion()
    print("No Motion...")
    pir.wait_for_motion()
    # if pir.motion_detected:
    print("\nMotion detected!\n")
    time.sleep(2)
